## Things I've Grokked over in the process...
* Capitalizing the waiting time for a job interview process that was supposed to last 3 weeks, but took almost 3 months...
* Grokking
* Separation of Concerns
* Loose Coupling, High Cohesion and Modularity
* SRP - Single Responsibility Principle
* Recursion and Iteration
* CRUD concepts from Rails
