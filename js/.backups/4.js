var Paws = Paws || { 'Kitty, Kitty' : 'Purrrrrrr....' };

// ;(function() {
	var model = {
		lastId: 0,
		__data__: [],
		// Create a new model and store it into the data array.
		addCat: function(name, image, template) { // Add Model (create) [object]
			return this.__data__.push({
				id: ++this.lastId,
				name: name,
				image: image || './img/display_placeholder.png',
				votes: 0,
				visible: true
			});
		},
		// Retrieve a model from the data array.
		get: function(id) {
			if (this.__data__[id]) { return this.__data__[id-1] };
		},

		// Retrieve all active models from the data array
		index: function(){
			return this.__data__.map(function(cat){
				return cat;
			});
		},
		init: function(){
		}
	};

	var octopus = {

		cat: function(cat) {
			return APP.view.addToList(APP.model.add(cat));
		},

		buildUI: function() {
		},

		init: function() {
			this.cat('Espanto', './img/display_placeholder.png');
			this.cat('Benito', './img/display_placeholder.png');
			this.cat('Cucho', './img/display_placeholder.png');
			this.cat('Demóstenes', './img/display_placeholder.png');
			this.cat('Panza', './img/display_placeholder.png');
			APP.model.init();
			this.buildUI();
		}
	};

	var view = {
		addToList: function(cat) {
			if((cat) && (typeof cat === 'object')){
				return (function(obj) {
					var elem = obj.view = {};
					elem.target = document.getElementById('list');
					elem.instance = document.createElement('li'),
					elem.instance.innerHTML = '<a href="#'+obj.name+'"><span>'+obj.name+'</span></a>';
					elem.target.appendChild(elem.instance);
					elem.instance.addEventListener('click', (function(catInstance) {
				      return function(){
				        view.displayImage(cat);
				        console.log('click ' + cat.name);
				      };
				    })(cat));
					return obj;
				}(cat));
			};
		},

		displayImage: function(cat) {
		    return function(cat) {
		      var display = document.getElementById('display');
		      display.innerHTML = '<img src="' + cat.image + '" alt="' + cat.id + '">';
		    };
		}
	};
// }());
