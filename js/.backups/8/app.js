(function() {
	'use strict';

	/**	MODEL
	---------------------------------------------------------------------------*/
	var model = {
		lastID: 0,
		data: [],
		// [model.create] Create a new item and store it in the data array
		create : function(name, image, imageRef) {
			if(name && typeof name === 'string') {
				model.data.push({
					id :  ++model.lastID,
					name : name,
					img : image,
					imgRef : imageRef,
					votes : 0,
					visible : true
				});
				return model.data[model.lastID - 1];
			}
		},

		// [model.get] Get a particular item from the data array
		get : function(idInt) {
			return model.data[idInt - 1];
		},

		// [model.change] Update info on existing item
		change : function(idInt, newParamsObj) {
			return (function(obj) {
				if(typeof idInt === 'number') {
					for(var i = 0; i < arguments.length; i++) {
						for (var prop in arguments[i]) {
							var value = arguments[i][prop];
							if (typeof value == "object") {
								model.change(obj[prop], value);
							} else {
								obj[prop] = value;
							}
						}
					}
					return obj;
				}
			}( model.get(idInt), newParamsObj) );
			// Inspired on: http://stackoverflow.com/a/12534361/2260466
		},

		// [model.destroy] Deletes a item (Just kiddin', only makes it non-visible)
		destroy : function(idInt) {
			if( model.get(idInt) ) {
				model.get(idInt).visible = false;
				return model.get(idInt);
			}
		},

		// [model.all] Get all 'visible' items from the data array.
		all: function() {
			var arr = [];
			model.data.forEach(function(item) {
				return arr.push(item);
			});
			return arr;
		}
	};

	/**	OCTOPUS
	---------------------------------------------------------------------------*/
	var octopus = {
		// [octopus.add] Add a new item - creates model and renders it.
		add: function(name, image, imageRef) {
			// TODO: Qué pasa si no le pasas un segundo argumento?
			// 			 Tal vez sea mejor recibir un objecto como model.update()
			if(arguments[0].length > 1 && typeof arguments[0] === 'string') {
					var itemObj = model.create(arguments[0], arguments[1]);
					UI.list.add(itemObj);
					UI.viewer.display(itemObj);
					return itemObj;
				} else {
					arguments[0].forEach(function(a) {
						octopus.add(a);
					});
			}
		},

		// [octopus.vote] takes an object and grow its vote count by one
		vote : function(number) {
			UI.viewer.voteCounter.innerHTML = ++model.get(number).votes;
		},

		// [octopus.api] Public access to the model data through a API-like-Object
		api : function() {
			// App Holder:
			APP.add = octopus.add;
		},

		// [octopus.init] Initializes the mighty octopus
		init : function() {
			octopus.api();
			UI.viewer.init();
			if( model.data.length ) {
				UI.viewer.display( model.get(model.lastID ));
			};
		}
	};

	/**	VIEW
	---------------------------------------------------------------------------*/
	var UI = {

		list: {
			target: document.getElementById( 'list' ),
			// [list.add] Renders a new DOM instance with the provided object.
			add: function(obj) {
				if(obj.visible) {
					var inst = document.createElement('li');
					inst.innerHTML = '<a href="#'+obj.id+'"><span>'+
						obj.name+'</span></a>';
					inst.addEventListener('click', (function(itemInstance) {
						return function() {
							UI.viewer.display(obj);
						};
					}( )));
					this.target.appendChild(inst);
					return obj;
				}
			},
			// [list.remove] remove a rendered item from the DOM
			remove: function(obj) {
				UI.list.target.removeChild(obj.listInstance);
			}
		},

		viewer: {
			DOMtarget: document.getElementById('displayImage'),
			voteCounter: document.getElementById('voteCounter'),
			displayName: document.getElementById('displayName'),
			currentitem: undefined,
			// [viewer.display]
			display: function(item) {
				this.currentitem = item.id;
				this.displayName.innerHTML = item.name;
				this.voteCounter.innerHTML = item.votes;
				this.DOMtarget.src = item.img;
			},
			// [viewer.init]
			init: function() {
				this.DOMtarget.addEventListener('click', (function(instance) {
					return function() {
						octopus.vote(instance.currentitem);
					};
				}(this)));
			}
		}

	};

	octopus.init();

}(window.APP = window.APP || {}));

// BATCH BUILD
(function() {
	console.groupCollapsed('Batch Build');
		console.log( APP.add('Espanto', 'img/Espanto.jpg') );
		console.log( APP.add('Cucho', 'img/Cucho.jpg') );
		console.log( APP.add('Benito', 'img/Benito.jpg') );
		console.log( APP.add('Demóstenes', 'img/Demóstenes.jpg') );
		console.log( APP.add('Panza', 'img/Panza.jpg') );
	console.groupEnd();
}( ));