var Paws = Paws || {};
;(function() {
	'use strict';
	var model = {
		current: 0,
		data: [],
		create : function(nameStr, imgStr, imgRefStr){
			if(nameStr && typeof nameStr === 'string'){
				model.data.push({
					id :  ++model.current,
					name : nameStr || 'Anonymous Cat',
					img : imgStr || 'img/placeholder.png',
					imgThumb : imgStr,
					imgRef : imgRefStr || 'Uncredited Image',
					votes : 0,
					visible : true
				});
			}
			return model.data[model.current-1];
		},
		read : function(idInt){
			return model.data[idInt-1];
		},
		update : function(idInt, editedObj){
			if(typeof idInt === 'number'){
				(function(obj) {
					for(var i = 0; i < arguments.length; i++) {
						for (var prop in arguments[i]) {
							var value = arguments[i][prop];
							if (typeof value == "object") {
								modify(obj[prop], value);
							} else {
								obj[prop] = value;
							}
						}
					}
					return obj;
				}(model.read(idInt), editedObj));
			}
		},
		destroy : function(idInt){
			if(model.read(idInt)){
				model.read(idInt).visible = false;
				return model.read(idInt);
			}
		},
		index: function(){
			var arr = [];
			model.data.forEach(function(cat){
				return arr.push(cat);
			});
			return arr;
		}
	};
	var octopus = {
		render: function(cat){
			if(arguments[0].length > 1 ) {
				arguments[0].forEach(function(a){
					octopus.render(a);
			});
				} else {
					cat.DOMinstance = view.list.add(cat);
			}
		},
		vote : function(cat){
			return ++cat.votes;
		},
		api : function(){
			console.log(Paws['Kitty, Kitty'] = 'Purrrrrrr.... ');
		},
		init : function(){
			octopus.api();
			tests.model();
			octopus.render(model.index());
		}
	};
	var view = {
		list: {
			target: document.getElementById( 'list' ),
			elements: [],
			add: function(obj){
				if(obj.visible){
					var inst = document.createElement('li');
					inst.innerHTML = '<a href="#"><span>'+obj.name+'</span></a>';
					inst.addEventListener('click', (function(catInstance) {
						return function(){
							view.display.update(obj);
						};
					})());
					this.target.appendChild(inst);
					return inst;
				}
			},
			remove: function(obj){
				view.list.target.removeChild(obj.DOMinstance);
			}
		},
		display: {
			target: document.getElementById('displayImage'),
			update: function(cat){
				this.target.src = cat.img;
			}
		}
	};
	var tests = {
		model: function(){
			console.groupCollapsed('Model CRUD Tests');
				console.log('Create (1) \n',
					model.create('Espanto', 'img/Espanto.png') );
				console.log('Create (2) \n',
					model.create('Cucho', 'img/Cucho.png') );
				console.log('Create (3) \n',
					model.create('Benito', 'img/Benito.png') );
				console.log('Create (4) \n',
					model.create('Demóstenes', 'img/Demóstenes.png') );
				console.log('Create (5) \n',
					model.create('Panza', 'img/Panza.png') );
				console.log('Create (6) \n',
					model.create('Matute', 'img/Matute.png') );
				console.log('Read (4) \n',
					model.read(4) );
				console.log('Destroy (6) \n',
					model.destroy(6) );
				console.log('Index \n',
					model.index() );
			console.groupEnd();
		}
	};
	octopus.init();
}());