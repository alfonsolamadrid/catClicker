// TODO: Why encapsulation in APP object?
var APP = APP || {};

// Why IIFE?
;(function () {
  'use strict'; // TODO: Why strict mode?

  // Cat Constructor
  function Cat(id, name, image){
    // Instance Information
    this.id = id;
    this.name = name || "Anonymous Feline";
    this.count = 0;
    this.image = image || 'img/placeholder.png';
    this.thumb = this.image || 'http://placehold.it/50';

    // DOM Element Setting and Rendering
    var sideList = document.getElementById('sideList');
    this.listItem = document.createElement('li');
    this.listItem.id = this.id;
    this.listItem.className = 'sideListItem';
    this.listItem.innerHTML =
    // <li id='this.listItem.id' class='this.listItem.className'>
        '<img class="sideListImage" src="' +
          this.thumb + '" alt="SideListImage"> \n' +
        '<span class="sideListText">' + this.name + '</span> \n' +
        '<label id="' + this.id +'Clicks">' + this.count + '</label>';
    // </li>
    this.renderListItem = function(a){
      return sideList.appendChild(a.listItem);
    }(this);

    this.clickCounter = document.getElementById(this.id + 'Clicks');

    // Object Methods
    this.vote = function(){
      return this.clickCounter.innerHTML = ++this.count;
    };

    // Click Event
    this.listItem.addEventListener('click', (function(catInstance) {
      return function(){
        APP.displayImage(catInstance);
      };
    })(this));
  };

  APP.displayImage = function(cat){
    return (function(){
      var displayContainer = document.getElementById('displayContainer');
      displayContainer.innerHTML = '<img src="' + cat.image + '" alt="' + cat.id + '">';
      // console.log(cat); // DEBUG
      cat.vote(); // Vote after rendering
    })();
  }

  window.newCat = function(id, name, image){
    return APP[id] = new Cat(id, name, image);
  };
}());

newCat('Cat1', 'Espanto', 'img/cat1.jpg');
newCat('Cat2', 'Benito', 'img/cat2.jpg');
newCat('Cat3', 'Cucho', 'img/cat3.jpg');
newCat('Cat4', 'Demóstenes');
newCat('Cat5', 'Panza');
console.log(APP);