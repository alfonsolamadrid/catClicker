var Paws = Paws || { 'Kitty, Kitty' : 'Purrrrrrr....' };

;(function() {
	var model = {
		lastId: 0,
		cats: [],
		Cat: function(nameStr, imageStr){
			this.name = nameStr;
			this.image = imageStr;
		},
		create: function(nameStr){
			return new this.Cat(nameStr);
		}
	};

	var octopus = {
		addCat: function(nameStr) {
			view.render(model.create( nameStr ));
		},
		init: function() {
			this.addCat('Espanto', './img/espanto.jpg');
			this.addCat('Benito', './img/benito.jpg');
			this.addCat('Cucho', './img/cucho.jpg');
			this.addCat('Demóstenes', './img/demóstenes.jpg');
			this.addCat('Panza', './img/panza.jpg');
		}
	};

	var view = {
		render: function(catObj) {
			console.log(catObj);
			if((catObj) && (typeof catObj === 'object')){
				return (function(obj) {
					var elem = obj.view = {};
					elem.target = document.getElementById('list');
					elem.instance = document.createElement('li'),
					elem.instance.innerHTML = '<a href="#'+obj.name+'"><span>'+obj.name+'</span></a>';
					elem.target.appendChild(elem.instance);
					elem.instance.addEventListener('click', (function(catInstance) {
				      return function(){
				        view.displayImage(obj);
				        console.log('click ' + obj.name);
				      };
				    })(catObj));
					return obj;
				}(catObj));
			};
		},

		displayImage: function(cat) {
		    return function(cat) {
		      var display = document.getElementById('display');
		      display.innerHTML = '<img src="' + cat.image + '" alt="' + cat.id + '">';
		    };
		}
	};
	octopus.init();
}());
