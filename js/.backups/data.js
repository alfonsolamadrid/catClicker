var app = app || {};

(function(){
	app.data = {
		lastId: 0,
		collection: [ ],
		add: function(name){
			if(name) {
				app.data.collection.push(new Cat(++app.data.lastId, name));
				return app.data.collection[app.data.lastId - 1];
			} else {
				return console.log('Please provide some sort of information
					about the cat you wish to create');
			};
		},
		remove : function(id){
			var cat = app.data.collection[id]
			cat.visible = false;
			return console.log(cat.name + ', id(' + cat.id + ') is now removed');
		},
		active : function(){
			var visibleCats = [];
			for(var cat in app.data.collection){
				if(app.data.collection[cat].visible){
					visibleCats.push(app.data.collection[cat]);
				}
			};
			return visibleCats;
		},
	};

	// Cat Constructor
	function Cat (id, name, image){
		this.id = id;
		this.name = name || 'Anonymous Feline' + this.id;
		this.image = image || 'http://placehold.it/250';
		this.clicks = 0;
		this.visible = true;
	};
}());