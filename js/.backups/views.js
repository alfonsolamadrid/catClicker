var app = app || {};

app.views = {
	item : {
		container: document.getElementById('itemView'),
		template: function(){
			return 'item view';
		}
	},
	list : {
		container: document.getElementById('listView'),
		template: function(id, name, image){
			return '<li class="item" id="'+id+'">' +
						'<img src="'+image+'" alt="'+name+'">' +
						'<span>'+name+'</span>' +
					'</li>';
		}
	}
};