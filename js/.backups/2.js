var Paws = { 'Kitty, Kitty' : 'Purrrrrrr....' };

;(function() { // NOTE: IIFE encapsulation with defensive semicolon.

	/**
	A. Data Model
	- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */
	var model = {
		lastId: 0,
		collection: [],
		new: function(name, image) { // Add Model (create) [object]
			this.collection.push({
				'id': ++model.lastId,
				'name': name,
				'image': image,
				'votes': 0,
				'visible': true
			});
			return this.collection[this.lastId-1];
		},
		get: function(id){ // Get Model (read) [object]
			return this.collection[id-1];
		},
		init: function(){ // model initializer
		}
	};

	/**
	B. Control Hub (Logical Controller)
	- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */
	var hub = {
		api: Paws,
		add: function(name, image){
			return view.build(model.new(name, image));
		},
		index: function(){
			var cats = model.collection,
				index = [];
			for (var i = 0; i < cats.length; i++) {
				index.push(cats[i]);
			};
			return index;
		},
		init: function(){
			this.api.add = this.add;
			// this.api.index = this.index;
			// this.api.get = model.get;
			// this.api.collection = model.collection;
			model.init();
			view.init();
		}
	};

	/**
	C. View
	- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */
	var view = {
		/** (C.1) Single Cat Full View */
		cat : {
			target: document.getElementById('cat'),
			render: function(){ },
			template: function(cat){ }
		},
		/** (C.2) List of Cats */
		list : {
			target: document.getElementById('list'),
			render: function(){ },
			template: function(array){ }
		},
		build: function(params){
			return params;
		},
		init: function(){ } // view initializer
	};

	hub.init();
}());

// NOTE: Generate the cats through the public api
console.log(Paws.add('Espanto', 'http://placehold.it/500'));
console.log(Paws.add('Benito', 'http://placehold.it/500'));
console.log(Paws.add('Cucho', 'http://placehold.it/500'));
console.log(Paws.add('Demóstenes', 'http://placehold.it/500'));
console.log(Paws.add('Panza', 'http://placehold.it/500'));

console.log(Paws);