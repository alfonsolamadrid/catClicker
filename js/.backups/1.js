render: function(element) {
		element.instance = document.createElement(element.type),
    	element.instance.innerHTML = element.template;
      	element.parent.appendChild(element.instance);
      	return element.template;
	},


// NOTAS:
// 1. Ojo con los closures en cats.active... checa el local scope E INVESTIGA

		/**
		(A.1) Add a new cat */
		add: function(name){
			if(name) {
				model.data.push(new model.Cat(++model.lastId, name));
				return model.data[model.lastId - 1];
			} else {
				return console.log('Please provide some sort of ' +
					'information about the cat you wish to create');
			};
		},
		/**
		(A.2) Show/retrieve a particular cat */
		get: function(id){
			return model.data[id-1];
		},
		/**
		(A.3) Remove a cat instance
		(without deleting it from the data array) */
		remove : function(id){
			var cat = model.data[id-1];
			if(cat.visible) {
				cat.visible = false;
				return cat;
			};
		},
		/**
		(A.4) Cat Model Constructor */
		Cat: function(id, name, image){
			this.id = id;
			this.name = name || 'Anonymous Feline #' + this.id;
			this.image = image || 'http://placehold.it/250';
			this.clicks = 0;
			this.visible = true;
		}

// if(!localstorage.cats) {localstorage.cats = JSON.stringify([ ])};

add: function(){
			return model.data.push({
				id: ++model.lastId,
				name: name,
				image: image
			});
		},
		update: function(){
			//
		},
		get: function(){
			//
		}
	};


	var control = {
		/**
		Retrieve an updated list of all active cat instances */
		getActiveCats : function(){
			cats.list = cats.list || [];
			var cats = model.data; // Only for readability
			for(var cat in cats){
				if(cats[cat].visible){
					cats.list.push(cats[cat]);
				}
			};
			console.log('Cat Collection Updated');
		},
		/**
		Retrieve a complete list of all created cats */
		getAllCats: function(){
			return model.data;
		},
		/**
		Controller Initializer */
		init: function(){
			// Purrrrrrr.... A Domestic-Feline Public API...
			cats.add 		= model.add;
			cats.show 		= model.get;
			cats.remove 	= model.remove;
			cats.collection = model.data;
			cats.showAll 	= control.showAll;
			cats.update 	= control.update;
		}
	};