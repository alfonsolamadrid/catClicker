// BATCH BUILD
(function() {
	console.groupCollapsed('Batch Build');
		console.log( APP.add('Espanto', 'img/Espanto.jpg', 'https://www.flickr.com/photos/bigtallguy/434164568') );
		console.log( APP.add('Cucho', 'img/Cucho.jpg', 'https://commons.wikimedia.org/wiki/File:Stray_kitten_Rambo002.jpg') );
		console.log( APP.add('Benito', 'img/Benito.jpg', 'https://www.flickr.com/photos/92414546@N04/15410576268/') );
		console.log( APP.add('Demóstenes', 'img/Demóstenes.jpg', 'https://www.flickr.com/photos/105567585@N06/10935543065') );
		console.log( APP.add('Panza', 'img/Panza.jpg', 'https://www.flickr.com/photos/evettesbu/3197489663/') );
	console.groupEnd();
}( ));