# Cat clicker
Cat clicker is a Frontend Development exercise, focused on javascript organizational best practices.

## Documentation
* [a. Project Specifications](docs/requirements.md)
* [b. Structure](docs/structure.md)
* [c. Process](docs/process.md)

```javascript
// From: js/batch.js
// (Populate this thing through API-like methods)
APP.add('Espanto', 'img/Espanto.jpg', 'https://www.flickr.com/photos/bigtallguy/434164568');
APP.add('Cucho', 'img/Cucho.jpg', 'https://commons.wikimedia.org/wiki/File:Stray_kitten_Rambo002.jpg');
APP.add('Benito', 'img/Benito.jpg', 'https://www.flickr.com/photos/92414546@N04/15410576268/');
APP.add('Demóstenes', 'img/Demóstenes.jpg', 'https://www.flickr.com/photos/105567585@N06/10935543065');
APP.add('Panza', 'img/Panza.jpg', 'https://www.flickr.com/photos/evettesbu/3197489663/');

```
